$(document).ready(function()
{

    $('.active-slider').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:5000,
        margin:10,
        nav:true,
        dots:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })




    // project slide

    $('.project-active').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:5000,
        margin:10,
        nav:true,
        dots:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:4
            }
        }
    })
   

    // video pop-up

    $('.video-popup').magnificPopup({
        type: 'iframe'
        // other options
      });

});